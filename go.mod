module gitlab.com/sercand/t2s

require (
	cloud.google.com/go v0.33.0
	cloud.google.com/go/compute/metadata v0.0.0-20181113205611-bb12be1742b2 // indirect
	github.com/dmulholland/mp3lib v0.0.0-20180826213248-fe8964d0532a
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	go.opencensus.io v0.18.0 // indirect
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
	google.golang.org/api v0.0.0-20181113174939-c5e41677a12e
	google.golang.org/genproto v0.0.0-20181109154231-b5d43981345b
)

go 1.13
