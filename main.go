package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	texttospeech "cloud.google.com/go/texttospeech/apiv1"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
	texttospeechpb "google.golang.org/genproto/googleapis/cloud/texttospeech/v1"
)

func newClient() *texttospeech.Client {
	ctx := context.Background()
	opts := []option.ClientOption{}
	if val := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS"); val != "" {
		opts = append(opts, option.WithCredentialsFile(val))
	} else {
		creds, err := google.FindDefaultCredentials(ctx, texttospeech.DefaultAuthScopes()...)
		if err == nil {
			opts = append(opts, option.WithCredentials(creds))
		}
	}
	c, err := texttospeech.NewClient(ctx, opts...)
	if err != nil {
		panic(err)
	}
	return c
}

func synthesize(client *texttospeech.Client, lang string, voice string, text string, out string) (*texttospeechpb.SynthesizeSpeechResponse, error) {
	req := &texttospeechpb.SynthesizeSpeechRequest{
		Input: &texttospeechpb.SynthesisInput{
			InputSource: &texttospeechpb.SynthesisInput_Text{
				Text: text,
			},
		},
		Voice: &texttospeechpb.VoiceSelectionParams{
			LanguageCode: lang,
			Name:         voice,
		},
		AudioConfig: &texttospeechpb.AudioConfig{
			Pitch:         0.0,
			SpeakingRate:  1.0,
			AudioEncoding: texttospeechpb.AudioEncoding_MP3,
		},
	}
	if strings.Contains(text, "<speak>") {
		req.Input = &texttospeechpb.SynthesisInput{
			InputSource: &texttospeechpb.SynthesisInput_Ssml{
				Ssml: text,
			},
		}
	}
	resp, err := client.SynthesizeSpeech(context.Background(), req)
	if err != nil {
		fmt.Println("api returned error", err.Error())
		return nil, err
	}
	if out != "" {
		return resp, ioutil.WriteFile(out, resp.AudioContent, 0755)
	}
	return resp, nil
}

func main() {
	lang := flag.String("language", "en-US", "language of text")
	voice := flag.String("voice", "en-US-Wavenet-C", "the voice. (en-US-Wavenet-C, en-GB-Wavenet-D)")
	flag.Parse()
	if len(flag.Args()) == 0 {
		panic("expected 1 argument")
	}
	fpath := flag.Arg(0)
	c := newClient()
	var data []byte
	var err error
	if fpath == "-" {
		data, err = ioutil.ReadAll(os.Stdin)
		if len(data) >= 10 {
			fpath = "stdin-" + string(data[0:10])
		} else {
			fpath = "stdin-" + string(data)
		}
		fpath = strings.Replace(fpath, " ", "-", -1)
	} else {
		data, err = ioutil.ReadFile(fpath)
	}
	if err != nil {
		panic(err)
	}
	output := fmt.Sprintf("%s-synthesized.mp3", strings.Replace(fpath, filepath.Ext(fpath), "", 1))
	fmt.Println("Synthesize to", *lang, "language with", *voice, "voice")

	if len(data) < 5000 {
		if _, err = synthesize(c, *lang, *voice, string(data), output); err != nil {
			panic(err)
		}
	} else {
		fmt.Println("text file has more than 5000 characters. It will split into chunks")
		var chunks []string
		all := bytes.Split(data, []byte("\n\n"))
		for _, a := range all {
			nc := len(a)
			if nc >= 5000 {
				panic("there is paragraph larger than 5000 characters")
			}
			if len(chunks) == 0 {
				chunks = append(chunks, string(a))
			} else {
				last := chunks[len(chunks)-1]
				n := len(last)
				if (n + nc) < 5000 {
					last += "\n" + string(a)
					chunks[len(chunks)-1] = last
				} else {
					chunks = append(chunks, string(a))
				}
			}
		}
		var flist []*texttospeechpb.SynthesizeSpeechResponse
		for i, chu := range chunks {
			fmt.Println("synthesize chunk", i+1, "It has", len(chu), "characters")
			if data, err := synthesize(c, *lang, *voice, chu, ""); err != nil {
				panic(err)
			} else {
				flist = append(flist, data)
			}
		}
		fmt.Println("\nMerging each chunk into one mp3 file...")
		merge(output, flist, true)
	}
	fmt.Println("Saved to", output)
}
